<!--get help https://getbootstrap.com/docs/4.0/components/modal/-->

<!------------------------------POP UP MODAL-------------------------------------->
<!-- https://wtmatter.com/php-mysql-crud/ add modal section -->

<div class="modal fade"
     id="addModal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <!--TITLE MODAL-->
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">What would you like to add?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--ADDING RECORD TO TABLE-->
            <form id="AddProduct" action="newProduct.php" method="POST">
                <div class="modal-body">

                    <!--name-->
                    <div class="form-group">
                        <label for="insertName">Name</label>
                        <input type="text" class="form-control" id="insertName" placeholder="Enter Name" name="name">
                    </div>

                    <!--description-->
                    <div class="form-group">
                        <label for="insertDescription">Description</label>
                        <input type="text" class="form-control" id="insertDescription" placeholder="Enter Description"
                               name="description">
                    </div>

                    <!--price-->
                    <div class="form-group">
                        <label for="insertPrice">Price</label>
                        <input type="number" min="0" class="form-control" id="insertPrice" placeholder="Enter Price"
                               name="price">
                    </div>

                    <!--picture-->
                    <div class="form-group">
                        <label for="insertPicture">Picture (ring,bangle,necklace..etc)</label>
                        <input type="text" class="form-control" id="insertPicture" placeholder="Enter Picture Source"
                               name="picture">
                    </div>
                </div>

                <!--buttons close and submit-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" value="Create" class="btn btn-success" id="submit" name="insertData">Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!------------------------------FOOTER-------------------------------------->
<!-- footer from la plage but product management not the store -->
<footer>
    <!-- https://getbootstrap.com/docs/4.0/components/buttons/ take button from here -->
    <button type="button" onclick="refreshTable()" id="refreshBtn" class="btn btn-primary btn-lg btn-block">Refresh
        Table
    </button>
    <!-- Button trigger modal -->
    <button type="button" id="newBtn" class="btn btn-secondary btn-lg btn-block" data-toggle="modal"
            data-target="#addModal">Create New Product
    </button>
    <br>
    <div class="text-center">

        <footer1>
            <div class="footer">

                <a><p> &copy; LA PLAGE Product management, jewelers since 1996 </p></a>
                <a title="Privacy policy">Privacy policy</a> | <a title="Code of ethics">Code of ethics</a>
                <p></p>
                <a href="https://github.com/LorineCherry">Lorine Cherry<img class="icon"
                                                                            src="https://image.flaticon.com/icons/svg/25/25231.svg"
                                                                            height="2%" width="2%"></a>
            </div>

        </footer1>

    </div>
</footer>
<!--------------------------------------------------------------------------->
<!-- https://wtmatter.com/php-mysql-crud/ -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <!--TITLE MODAL-->
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update values</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-------------UPDATE FORM------------->
            <form id="updateProduct" action="update.php" method="POST">
                <div class="modal-body">
                    <input type="hidden" id="hiddenID" name="id" value="">
                    <input type="hidden" name="dataUp" value="Update"/>
                    <!--name-->
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="nameUp">
                    </div>

                    <!--description-->
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" placeholder="Enter Description"
                               name="desUp">
                    </div>

                    <!--price-->
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" min="0" class="form-control" id="price" placeholder="Enter Price"
                               name="priceUp">
                    </div>

                    <!--picture-->
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="text" class="form-control" id="picture" placeholder="Enter Picture Source"
                               name="imageUp">
                    </div>

                </div>
                <!--buttons close and submit-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" name="close">Close</button>
                    <button type="submit" value="Update" class="btn btn-success" id="submitUpdate" name="dataUp">
                        Submit
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>