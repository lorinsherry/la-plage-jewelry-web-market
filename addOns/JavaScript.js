// -----------------------------SEARCH BAR------------------------
function searchBar() {

    var text, sort, table, tr, td, i, txtToSort;

    text = document.getElementById("searchBar");
    sort = text.value.toUpperCase();
    table = document.getElementById("productTable");
    tr = table.getElementsByTagName("tr");


    // search bar functionality

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtToSort = td.textContent || td.innerText;
            if (txtToSort.toUpperCase().indexOf(sort) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

// -----------------------------AJAX------------------------

function ajaxTable() {
    $.ajax({
        type: "POST",
        url: "config.php",
        success: function (data) {
            $('#productTable').html(data);
        }
    });
}

function refreshTable() {
    ajaxTable();
}

// -----------------------------UPDATE PRODUCT BY EDIT------------------------

$(document).ready(function () {
    $('body').on('click', '.editBtn', function () {
        var editID = $(this).attr('data-id');
        $tr = $(this).closest('tr');
        var data =$tr.children("td").map(function () {
            return $(this).text().trim() != '' ? $(this).text() : $(this).children('img').attr('src');
        }).get();
        console.log(data);
        //alert(data);
        $('#name').val(data[0]);
        $('#description').val(data[1]);
        $('#price').val(data[2]);
        $('#picture').val(data[3]);
        $("#hiddenID").val(editID);
    });
});

// ----------------------------------------------------------------------------------------
