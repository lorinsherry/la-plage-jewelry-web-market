<!DOCTYPE html>
<html lang="en">

<!------------------------------LINKS-------------------------------------->

<head>
    <meta charset="utf-8">
    <title>Jewlery | LA PLAGE</title>

    <!--BOOTSTRAP STYLE-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!--AJAX-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

    <!--JQUERY-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

    <!--SCRIPTS-->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!--LOADING MY FILES-->
    <link rel="stylesheet" href="addOns/Design.css">
    <script type="text/javascript" src="addOns/JavaScript.js"></script>

</head>

<!------------------------------LOGO + SEARCH BAR-------------------------------------->

<header id="Home">
    <div class="wrapper-logo float-left logo-container no-padding-desktop">
        <span class="simple-banner-component simple-banner">
            <a><h1><img title="LA PLAGE" alt="LA PLAGE" src="https://i.imgur.com/23rG3TI.png"
                                         width="200"
                                         height="40" class="center">
            <img src="./images/product_management.png" alt="product_management" width="500"
                 height="40" >
                    <img src="./images/sunset.png" alt="logo" width="100" height="60" >
            </a></span></h1>
        <div class="g">&copy;By Lorine Cherry</div>
        <p> </p>
    </div>
    <input type="text" id="searchBar" onkeyup="searchBar()" placeholder="What are you searching for?">
</header>

<!---------------------------------------------------------------------------------->