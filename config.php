<!--get help https://getbootstrap.com/docs/4.0/components/modal/-->
<?php
include 'DBConInfo.php';
?>

<!------------------------------UPDATE PRODUCTS MODAL-------------------------------------->

<body>


<!------------------------------UPDATED TABLE-------------------------------------->
<!-- https://getbootstrap.com/docs/4.1/content/tables/ -->

<form action="" method="POST">
    <table id="productTable" class="table table-hover table-dark">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Picture</th>
            <th colspan="2">Actions</th>
        </tr>
        </thead>

        <tbody id="tableBody">
        <?php
        $link = mysqli_connect("127.0.0.1", "root", "", "test");
        $result = mysqli_query($link, "SELECT * FROM product") or die(mysqli_error());

        while($row = mysqli_fetch_assoc($result))
        {
            echo "<tr>\n";?>
            <?php
            echo "<td>"; echo $row['name']; echo "</td>\n";
            echo "<td>"; echo $row['description']; echo "</td>\n";
            echo "<td>"; echo $row['price']; echo "</td>\n";
            echo "<td>"; echo "<img src='".$row['picture']."' /> "; echo "</td>\n";
            echo "<td>";
            ?>

            <a class="btn btn-secondary editBtn" data-id="<?php echo $row['id'] ?>" data-toggle='modal' data-target='#editModal' href="index.php?edit= <?php echo $row["id"]; ?>">EDIT</a>
            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
            <a class="btn btn-danger" href="deleteProduct.php?id= <?php echo $row["id"]; ?>">DELETE</a> <?php
            echo "</td>\n";
            echo "</tr>\n";
        }
        ?>
        </tbody>
    </table>
</form>
</body>